<?php

namespace OrderingBundle\Service;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Doctrine\ORM\EntityManagerInterface;
use OrderingBundle\Entity\Ordering;
use OrderingBundle\Entity\OrderingProduct;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class OrderingService
{
    private $em;
    private $container;

    private $customerRepository;
    private $productRepository;
    private $orderingRepository;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager, Container $container) {
        $this->em = $entityManager;
        $this->container = $container;

        $this->customerRepository = $this->em->getRepository('CustomerBundle:Customer');
        $this->productRepository = $this->em->getRepository('ProductBundle:Product');
        $this->orderingRepository = $this->em->getRepository('OrderingBundle:Ordering');
    }


    /**
     * A customer who has already bought for over € 1000, gets a discount of 10% on the whole order.
     *
     * @param array $items
     * @param int $discountPercentageValue
     * @param int $wholeSaleOver
     * @return int
     */
    private function discountValueOrder($items, $discountPercentageValue = 10, $wholeSaleOver=1000){
        $sumTotal = 0;
        $discountValue = 0;
        foreach ($items as $item){
            $sumTotal+=$item['total'];
        }

        if($sumTotal > $wholeSaleOver){
            $discountValue = $sumTotal*($discountPercentageValue/100);
        }
        return $discountValue;
    }

    /**
     * For every products of category "Switches" (id 2), when you buy five, you get a sixth for free.
     *
     * @param array $items
     * @param int $numberOfSwitches
     * @return int
     */
    private function discountSwitches($items, $numberOfSwitches=6){
        //getting info of products that have discount
        $infoProducts = $this->productRepository->findByCategory(2);
        $listProductsWithDiscount = array();
        foreach ($infoProducts as $infoProduct){
            $listProductsWithDiscount[] =$infoProduct->getId();
        }

        //get wich of the products have discount
        $totalValueDiscount = 0;
        foreach ($items as $item){
            if(in_array($item['product-id'], $listProductsWithDiscount)){
                if($item['quantity']>$numberOfSwitches){
                    $numberOfFreeeProducts =  floor($item['quantity'] / $numberOfSwitches);
                    $totalValueDiscount+= $numberOfFreeeProducts*$item['unit-price'];
                }
            }
        }

        return $totalValueDiscount;
    }

    /**
     * If you buy two or more products of category "Tools" (id 1), you get a 20% discount on the cheapest product.
     *
     * @param array $items
     * @param int $discountValue
     * @param int $wholeSaleOver
     * @return int
     */
    private function discountProductsCategoryTools($items){
        $valueDiscount = 0;

        //getting info of products that have discount
        $infoProducts = $this->productRepository->findByCategory(1);
        $listProductsWithDiscount = array();
        foreach ($infoProducts as $infoProduct){
            $listProductsWithDiscount[] =$infoProduct->getId();
        }


        $listPricesProducts = array('quantity'=>0, 'listPrices'=>array());
        foreach ($items as $item) {
            if (in_array($item['product-id'], $listProductsWithDiscount)) {
                $listPricesProducts['quantity']+=$item['quantity'];
                $listPricesProducts['listPrices'][] = $item['unit-price'];
            }
        }

        if(
            $listPricesProducts['quantity'] >= 2 &&
            count($listPricesProducts['listPrices']) > 0
        ){
            ksort($listPricesProducts['listPrices']);
            $valueDiscount = floatval($listPricesProducts['listPrices'][0])*(0.2);
        }

        return $valueDiscount;
    }
    private function calculateDiscounts($items){
        $valueDiscountValueOrder = $this->discountValueOrder($items);
        $valueDiscountSwitches = $this->discountSwitches($items);
        $valueDiscountProductsCategoryTools = $this->discountProductsCategoryTools($items);

        return $valueDiscountValueOrder+$valueDiscountSwitches+$valueDiscountProductsCategoryTools;
    }

    public function saveOrdering($customerId, $items, $total) {
        $customer = $this->customerRepository->findOneById($customerId);

        $ordering = new Ordering();
        $ordering->setCustomer($customer);
        $ordering->setTotal($total);
        $ordering->setDiscount($this->calculateDiscounts($items));
        $this->em->persist($ordering);
        $this->em->flush();

        if(count($items > 0)){
            foreach ($items as $item){
                $productId = $this->productRepository->findOneById($item["product-id"]);

                $orderingProduct = new OrderingProduct();
                $orderingProduct->setOrdering($ordering);
                $orderingProduct->setProduct($productId);
                $orderingProduct->setQuantity($item["quantity"]);
                $orderingProduct->setUnitprice($item["unit-price"]);
                $orderingProduct->setTotal($item["total"]);
                $this->em->persist($orderingProduct);
                $this->em->flush();
            }
        }

        return $ordering;
    }

}
