<?php

namespace OrderingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderingItem
 *
 * @ORM\Table(name="ordering_product")
 * @ORM\Entity(repositoryClass="OrderingBundle\Repository\OrderingItemRepository")
 */
class OrderingProduct
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Ordering", inversedBy="orderingProduct")
     * @ORM\JoinColumn(name="orderingId", referencedColumnName="id")
     */
    private $ordering;

    /**
     *
     * @ORM\ManyToOne(targetEntity="ProductBundle\Entity\Product")
     * @ORM\JoinColumn(name="productId", referencedColumnName="idProduct")
     */
    private $product;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity", type="integer")
     */
    private $quantity;

    /**
     * @var float
     *
     * @ORM\Column(name="unitprice", type="float")
     */
    private $unitprice;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float")
     */
    private $total;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return OrderingProduct
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set unitprice
     *
     * @param float $unitprice
     *
     * @return OrderingProduct
     */
    public function setUnitprice($unitprice)
    {
        $this->unitprice = $unitprice;

        return $this;
    }

    /**
     * Get unitprice
     *
     * @return float
     */
    public function getUnitprice()
    {
        return $this->unitprice;
    }

    /**
     * Set total
     *
     * @param float $total
     *
     * @return OrderingProduct
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set ordering
     *
     * @param \OrderingBundle\Entity\Ordering $ordering
     *
     * @return OrderingProduct
     */
    public function setOrdering(\OrderingBundle\Entity\Ordering $ordering = null)
    {
        $this->ordering = $ordering;

        return $this;
    }

    /**
     * Get ordering
     *
     * @return \OrderingBundle\Entity\Ordering
     */
    public function getOrdering()
    {
        return $this->ordering;
    }

    /**
     * Set product
     *
     * @param \ProductBundle\Entity\Product $product
     *
     * @return OrderingProduct
     */
    public function setProduct(\ProductBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \ProductBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
