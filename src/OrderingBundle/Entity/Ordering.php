<?php

namespace OrderingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ordering
 *
 * @ORM\Table(name="ordering")
 * @ORM\Entity(repositoryClass="OrderingBundle\Repository\OrderingRepository")
 */
class Ordering
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * One Order has One Customer.
     * @ORM\ManyToOne(targetEntity="CustomerBundle\Entity\Customer")
     * @ORM\JoinColumn(name="customerId", referencedColumnName="id")
     */
    private $customer;

    /**
     * @ORM\OneToMany(targetEntity="OrderingProduct", mappedBy="ordering", cascade={"persist"})
     */
    private $orderingProduct;

    /**
     * @var float
     *
     * @ORM\Column(name="discount", type="float")
     */
    private $discount;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float")
     */
    private $total;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orderingProduct = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set total
     *
     * @param float $total
     *
     * @return Ordering
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set customer
     *
     * @param \CustomerBundle\Entity\Customer $customer
     *
     * @return Ordering
     */
    public function setCustomer(\CustomerBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \CustomerBundle\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Add orderingProduct
     *
     * @param \OrderingBundle\Entity\OrderingProduct $orderingProduct
     *
     * @return Ordering
     */
    public function addOrderingProduct(\OrderingBundle\Entity\OrderingProduct $orderingProduct)
    {
        $this->orderingProduct[] = $orderingProduct;

        return $this;
    }

    /**
     * Remove orderingProduct
     *
     * @param \OrderingBundle\Entity\OrderingProduct $orderingProduct
     */
    public function removeOrderingProduct(\OrderingBundle\Entity\OrderingProduct $orderingProduct)
    {
        $this->orderingProduct->removeElement($orderingProduct);
    }

    /**
     * Get orderingProduct
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrderingProduct()
    {
        return $this->orderingProduct;
    }

    /**
     * Set discount
     *
     * @param float $discount
     *
     * @return Ordering
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }
}
