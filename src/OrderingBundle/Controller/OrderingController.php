<?php

namespace OrderingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;

class OrderingController extends Controller
{
    /**
     * Get All Orders in database
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Get All orders in database",
     * )
     */
    public function getAllAction()
    {
        $repository = $this->getDoctrine()->getRepository('OrderingBundle:Ordering');
        return $repository->findAll();
    }

    /**
     * Create an order with products
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Create an order with products",
     *  requirements={
     *      {
     *          "name"="customer-id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Customer id"
     *      },
     *     {
     *          "name"="items",
     *          "dataType"="string",
     *          "description"="List of item"
     *      },
     *     {
     *          "name"="total",
     *          "dataType"="float",
     *          "requirement"="[+-]?([0-9]*[.])?[0-9]+",
     *          "description"="Total Value of Order"
     *      },
     *  }
     * )
     */
    public function postAction(Request $request)
    {
        $customerId = $request->get('customer-id');
        $items = json_decode($request->get('items'), true);
        $total = $request->get('total');

        $orderingService = $this->get('ordering.service');
        $orderingResult = $orderingService->saveOrdering($customerId, $items, $total);
        return new Response($orderingResult);

    }
}
