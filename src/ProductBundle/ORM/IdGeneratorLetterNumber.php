<?php
namespace ProductBundle\ORM;

use Doctrine\ORM\Id\AbstractIdGenerator;
class IdGeneratorLetterNumber extends AbstractIdGenerator
{
    public function generate(\Doctrine\ORM\EntityManager $em, $entity)
    {
        // Create id here
        $id = uniqid();
        return $id;
    }
}