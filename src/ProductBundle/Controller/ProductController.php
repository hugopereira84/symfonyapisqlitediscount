<?php

namespace ProductBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class ProductController extends Controller
{
    /**
     * Get All Products in database
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Get All Products in database",
     * )
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()->getRepository('ProductBundle:Product');
        return $repository->findAll();
    }
}
