<?php
namespace ProductBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ProductBundle\Entity\Product;

class LoadProductData implements FixtureInterface
{
    /**
     * data seed: php bin/console doctrine:fixtures:load
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $allInfo = array(
            array(
                "A101",
                "Screwdriver",
                "1",
                "9.75"
            ),

            array(
                "A102",
                "Electric screwdriver",
                "1",
                "49.50"
            ),
            array(
                "B101",
                "Basic on-off switch",
                "2",
                "4.99"
            ),

            array(
                "B102",
                "Press button",
                "2",
                "4.99"
            ),
            array(
                "B103",
                "Switch with motion detector",
                "2",
                "12.95"
            )
        );



        foreach($allInfo as $info){
            $product = new Product();
            $product->setId($info[0]);
            $product->setDescription($info[1]);
            $product->setCategory($info[2]);
            $product->setPrice($info[3]);

            $manager->persist($product);
            $manager->flush();
        }
    }
}