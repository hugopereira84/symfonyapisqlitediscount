<?php

namespace CustomerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class CustomerController extends Controller
{
    /**
     * Get All Customers in database
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Get All Customers in database",
     * )
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()->getRepository('CustomerBundle:Customer');
        $products = $repository->findAll();
        
        return $products;
    }
}
