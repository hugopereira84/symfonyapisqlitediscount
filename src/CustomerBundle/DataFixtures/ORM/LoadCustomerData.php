<?php
namespace CustomerBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use CustomerBundle\Entity\Customer;

class LoadCustomerData implements FixtureInterface
{
    /**
     * data seed: php bin/console doctrine:fixtures:load
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $allInfo = array(
            array(
                "Coca Cola",
                "2014-06-28",
                "492.12"
            ),

            array(
                "Teamleader",
                "2015-01-15",
                "1505.95"
            ),
            array(
                "Jeroen De Wit",
                "2016-02-11",
                "0.00"
            )
        );



        foreach($allInfo as $info){
            $customer = new Customer();
            $customer->setName($info[0]);
            $customer->setSince(new \DateTime($info[1]));
            $customer->setRevenue($info[2]);

            $manager->persist($customer);
            $manager->flush();
        }
    }
}