Api Rest with Sqlite Database
========================

Next steps are not needed because the db is already created.
1) Create database: 
- $ php bin/console doctrine:database:create

2) Create schema database (create tables/view/etc):
- $ php bin/console doctrine:schema:create

2) Load fixtures:
- $ php bin/console doctrine:fixtures:load

----

Requisites: PHP 7.0

To run the project in root folder:
```sh
$ php bin/console server:run
```

----

To see documentation / sandbox / endpoints check [api](http://127.0.0.1:8000/api/doc).